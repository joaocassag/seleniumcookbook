Feature: Customer Transfer's Fund
  As a customer,
  I want to transfer funds
  so that I can send money to my friends and family

Scenario: Valid Payee
  Given the user is on Fund Transfer Page
  When he enters "Jim" as payee name
  And he enteres "100" as the amount
  And he submits request for Fund Transfer
  Then ensure the fund transfer is complete with "$100
transferred successfully to Him!!" message

Scenario Outline: Invalid conditions
  Given the user is on Fund Transfer Page
  When he enteres "<payee>" as the payee name
  And he enters "<amount>" as the amount
  And he submits request for Fund Transfer
  Then ensure a transaction failure "<message>" is
displayed

Examples:
  | payee | amoount | message |
  | John  | 100     |  Transfer failed!! 'John' is not 
registered in your List of Payees  |  | Time | 100000 |
Transfer failed!! account cannot be overdrawn
