/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secookbook.example.chapter8;
import com.secookbook.example.chapter8.pageobjects.BmiCalcPage;
import org.junit.After;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class BmiCalculatorRefactoredTest {
        
    private WebDriver driver;
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        driver = new FirefoxDriver();
        //driver.get("http://cookbook.seleniumacademy.com/bmicalculator.html");
    }
    
    @Test
    public void testBmiCalculation(){
        BmiCalcPage bmiPage = new BmiCalcPage(driver);
        //bmiPage.load();
        bmiPage.get();
        bmiPage.calculateBmi("181", "80");
        
        assertEquals("24.4", bmiPage.getBmi());
        assertEquals("Normal", bmiPage.getBmiCategory());
        
        bmiPage.close();
    }
    
    @After
    public void tearDown(){
        driver.quit();
    }
    
}
