
package com.secookbook.example.chapter8;

import com.secookbook.example.chapter8.pageobjects.Browser;
import com.secookbook.example.chapter8.pageobjects.HomePage;
import com.secookbook.example.chapter8.pageobjects.SearchResults;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class SearchTest {
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
    }
    
    @Test
    public void testProductSearch(){
        try{
            HomePage homePage = new HomePage(Browser.driver());
            homePage.get();
            
            SearchResults searchResult = homePage.Search().searchInStore("phones");
            
            assertEquals(2, searchResult.getProducts().size());
            assertTrue(searchResult.getProducts().contains("MADISON OVEREAR HEADPHONES"));
        } finally{
            Browser.close();
        }
    }
    
}
