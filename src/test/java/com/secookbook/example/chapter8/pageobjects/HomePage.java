
package com.secookbook.example.chapter8.pageobjects;

import static org.junit.Assert.assertEquals;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class HomePage extends LoadableComponent<HomePage> {
    
    private WebDriver driver;
    static String url="http://demo.magentocommerce.com/";
    //private static String title = "Madison Island";
    private static String title = "Open Source | eCommerce Software | Magento";
    
    public HomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    protected void load() {
        Browser.open(url);
    }

    @Override
    protected void isLoaded() throws Error {
        assertEquals(title,driver.getTitle());
    }
    
    public Search Search(){
        Search search = new Search(driver);
        return search;
    }
    
}
