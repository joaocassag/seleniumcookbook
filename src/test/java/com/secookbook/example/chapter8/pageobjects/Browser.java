
package com.secookbook.example.chapter8.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class Browser {
    
    
    private static WebDriver driver;
    
    public static WebDriver driver(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        if (driver ==null){driver = new FirefoxDriver();}
        return driver;
    }
    
    public static void open(String url){
        driver.get(url);
    }
    
    public static void close(){
        driver.quit();
    }
    
}
