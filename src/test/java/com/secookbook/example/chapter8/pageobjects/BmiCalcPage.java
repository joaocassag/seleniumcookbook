
package com.secookbook.example.chapter8.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.LoadableComponent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class BmiCalcPage extends LoadableComponent<BmiCalcPage> {
    
    //@FindBy(id="heightCMS")
    //@CacheLookup
    private WebElement heightCMS;
    private WebElement weightKg;
    private WebElement Calculate;
    private WebElement bmi;
    
    @FindBy(id="bmi_category")
    private WebElement bmiCategory;
    
    private WebDriver driver;
    
    private String url = "http://cookbook.seleniumacademy.com/bmicalculator.html";
    private String title = "BMI Calculator";
    
    public BmiCalcPage(WebDriver driver){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    
    public BmiCalcPage(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        this.driver = new FirefoxDriver();
        PageFactory.initElements(driver, this);
    }
    
    public void load(){
        this.driver.get(url);
    }
    
    protected void isLoaded() throws Error{
        assertTrue("Bmi Calculator page not loaded",driver.getTitle().equals(title));
    }
    
    public void close(){
        driver.quit();
    }
    
    public void calculateBmi(String height, String weight){
        heightCMS.sendKeys(height);
        weightKg.sendKeys(weight);
        Calculate.click();
    }
    
    public void setHeight(String height){
        heightCMS.sendKeys(height);
    }
    
    public void setWeight(String weight){
        weightKg.sendKeys(weight);
    }
    
    public void calculateBmi(){
        Calculate.click();
    }
    
    public String getBmi(){
        return bmi.getAttribute("value");
    }
    
    public String getBmiCategory(){
        return bmiCategory.getAttribute("value");
    }
    
}
