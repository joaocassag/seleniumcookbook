
package com.secookbook.example.chapter8;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.secookbook.example.chapter8.pageobjects.BmiCalcPage;
import static org.junit.Assert.assertEquals;
import org.junit.Ignore;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class BmiCalculatorTest {
    
    private WebDriver driver;
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        driver = new FirefoxDriver();
        driver.get("http://cookbook.seleniumacademy.com/bmicalculator.html");
    }
    
    @Test
    public void testBmiCalculation(){
        BmiCalcPage bmiPage = new BmiCalcPage(driver);
        
        bmiPage.setHeight("181");
        bmiPage.setWeight("80");
        bmiPage.calculateBmi();
        
        assertEquals("24.4", bmiPage.getBmi());
        assertEquals("Normal", bmiPage.getBmiCategory());
    }
    
    @After
    public void tearDown(){
        driver.quit();
    }
    
    
}
