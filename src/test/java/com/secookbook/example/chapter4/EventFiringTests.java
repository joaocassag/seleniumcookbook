
package com.secookbook.example.chapter4;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.*;
import org.apache.commons.io.FileUtils;

import static org.junit.Assert.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class EventFiringTests {
    private WebDriver driver;
    
    @Before
    public void setUp() throws Exception{
        //TODO: select driver based on platform
        System.setProperty("webdriver.gecko.driver","src/drivers/geckodriver_mac");
        driver = new FirefoxDriver();
    }
    
    @Test
    @Ignore
    public void testEventFiringWedDriver(){
        EventFiringWebDriver eventDriver = new EventFiringWebDriver(driver);
        MyListener myListener = new MyListener();
        eventDriver.register((WebDriverEventListener) myListener);
        
        eventDriver.get("http://bit.ly/1DbdhsW");
        eventDriver.findElement(By.id("q")).sendKeys("Selenium Testing Tools Cookbook");
    }
    
    @After
    public void tearDown() throws Exception{
        driver.quit();
    }
    
}
