
package com.secookbook.example.chapter4;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.events.WebDriverListener;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class MyListener implements WebDriverListener {
    
    public void beforeChangeValueOf(WebElement element, WebDriver driver){
        element.clear();
    }
    
    public void onException(Throwable exception, WebDriver driver){
        try{
            if(driver.getClass().getName().equals("org.openqa.selenium.remote.RremoteWebDriver")){
                driver = new Augmenter().augment(driver);
            }
            File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(file, new File("target/screenshots/error.png"));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
