
package com.secookbook.example.chapter4;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.*;
import org.apache.commons.io.FileUtils;

import static org.junit.Assert.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class SeleniumAPITest {
    
    WebDriver driver;
    
    @Before
    public void setUp(){
        //TODO: select driver based on platform
        System.setProperty("webdriver.gecko.driver","src/drivers/geckodriver_mac");
        
        driver = new FirefoxDriver();
        //driver = new RemoteWebDriver(capabilities);
        //driver = new ChromeDriver();
        driver.manage().window().maximize();
        
    }
    
    @Test
    @Ignore
    public void testDoubleClick() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/DoubleClickDemo.html");
        
        WebElement message = driver.findElement(By.id("message"));

        //assertEquals("blue",message.getCssValue("background-color"));
        //assertEquals("rgba(0,0,255,1)",message.getCssValue("background-color"));
        //System.out.println("BACKGROUND COLOR IS: "+message.getCssValue("background-color"));
        assertEquals("rgb(0, 0, 255)",message.getCssValue("background-color"));
        //rgb(0,[ 0, ]255)

        Actions builder = new Actions(driver);
        builder.doubleClick(message).perform();

        //assertEquals("rgba(255, 255,0,1)",message.getCssValue("background-color"));
        assertEquals("rgb(255, 255, 0)",message.getCssValue("background-color"));
        
    }
    
    @Test
    @Ignore
    public void testDragDrop() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/DragDropDemo.html");
        
        WebElement source = driver.findElement(By.id("draggable"));
        WebElement target = driver.findElement(By.id("droppable"));
        
        Actions builder = new Actions(driver);
        builder.dragAndDrop(source, target).perform();
        assertEquals("Dropped!",target.getText());
    }
    
    @Test
    @Ignore
    public void testContextMenu() throws Exception{
        WebElement clickMeElement = driver.findElement(By.cssSelector("div.context-menu-one.box.menu-1"));
        WebElement editMenuItem = driver.findElement(By.cssSelector("li.context-menu-item.icon-edit"));
        
        Actions builder = new Actions(driver);
        builder.contextClick(clickMeElement)
                .moveToElement(editMenuItem)
                .click().perform();
        
        WebDriverWait wait = new WebDriverWait(driver,10);
        
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        assertEquals("clicked: edit",alert.getText());
        alert.dismiss();
    }
    
    @Test
    @Ignore
    public void testContextMenuWithKeys() throws Exception{
        WebElement clickMeElement = driver.findElement(By.cssSelector("div.context-menu-one.box.menu-1"));
        
        Actions builder = new Actions(driver);
        builder.contextClick(clickMeElement)
                .sendKeys(Keys.chord(Keys.ALT,"e"))
                .perform();
        
        WebDriverWait wait = new WebDriverWait(driver,10);
        
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        assertEquals("clicked: edit",alert.getText());
        alert.dismiss();
    }
    
    @Test
    @Ignore
    public void testJavaScriptCalls() throws Exception{
        driver.get("http://google.com");
        
         new WebDriverWait(driver,5).until(new ExpectedCondition<Boolean>(){
            public Boolean apply(WebDriver d){
                return d.getTitle().
                        toLowerCase().
                        startsWith("google");
            }
        });
        
         JavascriptExecutor js = (JavascriptExecutor) driver;
         
        System.out.println("TITLE#6");
        String title = (String) js.executeScript("var aN = document.title; return aN");//???
        System.out.println("TITLE#9  IS: "+title);
        assertTrue("google".equalsIgnoreCase(title));
        
        //System.out.println("TITLE#8");
        long links = (Long) js.executeScript("var links = "
                + "document.getElementsByTagName('A'); return links.length");
        System.out.println("NUMBER OF LINKS  IS: "+links);
        assertEquals(18,links);
        
    }
    
    @Test
    @Ignore
    public void testTakeScreenshot() throws Exception{
        driver.get("http://google.com");
        File scrfile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrfile, new File("target/main_Page.png"));
        
        //RemoteWebDriver does not have TakesScreenshot, but augmenter does:
        // driver = new Augmenter().augment(driver);
    }
    
    @Test
    @Ignore
    public void testCookies() throws Exception{
        driver.get("http://demo.magentocommerce.com/");
        
        Select language = new Select(driver.findElement(By.id("select-language")));
        
        assertEquals("English",language.getFirstSelectedOption().getText());
        
        Cookie storeCookie = driver.manage().getCookieNamed("store");
        
        language.selectByVisibleText("French");
        
        storeCookie = driver.manage().getCookieNamed("store");
        assertEquals("french",storeCookie.getValue());
    }
    
    @Test
    @Ignore
    public void testNavigation() throws Exception{
        driver.get("http://google.com");
        
        WebElement searchField = driver.findElement(By.name("q"));
        searchField.clear();
        
        searchField.sendKeys("selenium webdriver");
        searchField.submit();
        
        WebElement resultLink = driver.findElement(By.linkText("selenium"));
        resultLink.click();
        
        new WebDriverWait(driver,10).until(ExpectedConditions.titleContains("selenium"));
        assertEquals("Selenium WebDriver", driver.getTitle());
        
        driver.navigate().back();
        
        new WebDriverWait(driver,10).until(ExpectedConditions.titleContains("selenium"));
        assertEquals("selenium webDriver - Google Search", driver.getTitle());
        
        driver.navigate().forward();
        
        new WebDriverWait(driver,10).until(ExpectedConditions.titleContains("selenium webdriver"));
        assertEquals("Selenium WebDriver", driver.getTitle());
        
        driver.navigate().refresh();
        
        new WebDriverWait(driver,10).until(ExpectedConditions.titleContains("selenium webdriver"));
        assertEquals("Selenium WebDriver", driver.getTitle());
    }
    
    @After
    public void tearDown() throws Exception{
        driver.quit();
    }
    
}
