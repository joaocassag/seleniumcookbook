
package com.secookbook.example.chapter1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.*;

import static org.junit.Assert.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class GoogleSearchTest {
    
    private WebDriver driver;
    
    @Before
    public void setUp(){
        //TODO: select driver based on platform
        System.setProperty("webdriver.gecko.driver","src/drivers/geckodriver_mac");
        
        driver = new FirefoxDriver();
        //driver = new RemoteWebDriver(capabilities);
        //driver = new ChromeDriver();
        driver.manage().window().maximize();
        
    }
    
    @Test
    @Ignore
    public void testGoogleSearch(){
        driver.get("http://www.google.com");
        WebElement element = driver.findElement(By.name("q"));
        element.clear();
        element.sendKeys("Selenium testing tools cookbook");
        element.submit();
        
        new WebDriverWait(driver,10).until(new ExpectedCondition<Boolean>(){
            public Boolean apply(WebDriver d){
                return d.getTitle().
                        toLowerCase().
                        startsWith("selenium testing tools cookbook");
            }
        });
        assertEquals("Selenium testing tools cookbook - Google Search",driver.getTitle());
    }
    
    @Test
    @Ignore
    public void testDefaultSelectedCheckbox(){
        List<String> checked = Arrays.asList("user1","user2");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        
        @SuppressWarnings("unchecked")
        List<WebElement> elements = (List<WebElement>) js.executeScript("return jQuery.find(':checked')");
        
        assertEquals(elements.size(),2);
        
        for(WebElement element: elements){
            assertTrue(checked.contains(element.getAttribute("id")));
        }
    }
    
    @Test
    @Ignore
    public void testElementText(){
        WebElement message = driver.findElement(By.id("message"));
        String messageText = message.getText();
        assertEquals("Click on me and my color will change",messageText);
        
        WebElement area = driver.findElement(By.id("area"));
        assertEquals("Div's Text\nSpan's Text",area.getText());
        
        //assertEquals("Div's Text\nSpan's Text",area.getAttribute("innerText")); //innerText attribute not support in Firefox?
    }
    
    @Test
    @Ignore
    public void testElementAttribute(){
        WebElement message = driver.findElement(By.id("message"));
        assertEquals("justify",message.getAttribute("align"));
    }
    
    @Test
    @Ignore
    public void testElementStyle(){
        WebElement message = driver.findElement(By.id("message"));
        String width = message.getCssValue("width");
        assertEquals("150px",width);
    }
    
    @Test
    @Ignore
    public void testDropdown(){
        Select make = new Select(driver.findElement(By.name("name")));
        
        assertFalse(make.isMultiple());
        assertEquals(4,make.getOptions().size());
        
        List<String> expectedOptions = Arrays.asList("BMW","Merceds","Audi","Honda");
        List<String> actualOptions = new ArrayList<String>();
        
        for(WebElement option: make.getOptions()){
            actualOptions.add(option.getText());
        }
        
        assertArrayEquals(expectedOptions.toArray(),actualOptions.toArray());
        
        make.selectByVisibleText("Honda");;
        assertEquals("someText", make.getFirstSelectedOption().getText());
        
        make.selectByValue("audi");
        assertEquals("aValue", make.getFirstSelectedOption().getText());
        
        make.selectByIndex(0);
        assertEquals("BMW", make.getFirstSelectedOption().getText());
    }
    
    @Test
    @Ignore
    public void testMultipleSelectList(){
        Select color = new Select(driver.findElement(By.name("color")));
        assertTrue(color.isMultiple());
        assertEquals(4,color.getOptions().size());
        
        color.selectByVisibleText("Red");
        color.selectByVisibleText("Green");
        color.selectByVisibleText("Blue");
        
        color.deselectByValue("rd");
        assertEquals(1,color.getAllSelectedOptions().size());
        
        color.deselectByIndex(0);
        assertEquals(0,color.getAllSelectedOptions().size());
    
    }
    
    @Test
    @Ignore
    public void testRadioButton(){
        WebElement petrol = driver.findElement(By.xpath("// input[@value='Petrol']"));
        if(!petrol.isSelected()){
            petrol.click();
        }
        
        assertTrue(petrol.isSelected());
        
        List<WebElement> fuelType = driver.findElements(By.name("fuel_type"));
        for(WebElement type: fuelType){
            if(!type.getAttribute("value").equals("Diesel")){
                type.click();
            }
            assertTrue(type.isSelected());
            break;
        }
    }
    
    @Test
    @Ignore
    public void testCheckBox(){
        WebElement airbags = driver.findElement(By.xpath("//input[]@value='Aribags'"));
        if(!airbags.isSelected()){
            airbags.click();
        }
        assertTrue(airbags.isSelected());
        
        if(airbags.isSelected()){
            airbags.click();
        }
        assertFalse(airbags.isSelected());
    }
    
    @Test
    @Ignore
    public void testWebTable(){
        WebElement simpleTable = driver.findElement(By.id("items"));
        List<WebElement> rows = simpleTable.findElements(By.tagName("tr"));
        assertEquals(3,rows.size());
        
        for(WebElement row: rows){
            List<WebElement> cols = row.findElements(By.tagName("td"));
            for(WebElement col: cols){
                System.out.print(col.getText()+"\t");
            }
            System.out.println();
        }
        
        //Find elements using css selectors and xpath
        //WebElement cell = driver.findElement(By.cssSelector("table#items tbody tr:nth-child(2)"));
        //WebElement cell = driver.findElement(By.xpath(" //table[@id='items'/tbody/tr[2]/td"));
    }
    
    @Test
    @Ignore
    public void testIsElementPresent(){
        if(isElementPresent(By.name("airbags"))){
            WebElement airbag = driver.findElement(By.name("airbag"));
            if(!airbag.isSelected()){
                airbag.click();
            } else{
                fail("Airbag checkbox does not exist");
            }
        }
    }
    
    @Test
    @Ignore
    public void testElementIsEnabled(){
        WebElement el = driver.findElement(By.name("el"));
        if(el.isEnabled()){
            if(el.isSelected()){
                el.click();
            }
            else{
                fail("el checkbox is disable");
            }
        }
    }
    
    @Test
    @Ignore
    public void testRowSelectionUsingControlKey(){
        List<WebElement> tableRows = driver.findElements(By.xpath("// table[@class='iceTable']/tbody/tr"));
        
        Actions builder = new Actions(driver);
        builder.click(tableRows.get(0))
                .keyDown(Keys.CONTROL)
                .click(tableRows.get(1))
                .keyUp(Keys.CONTROL)
                .build().perform();
        
        List<WebElement> rows = driver.
                findElements(
                        By.xpath("// div[@class='iceLineExample']/table[@class='iceTable']/tbody/tr"));
        assertEquals(2,rows.size());
    }
    
    @After
    public void tearDown() throws Exception{
        driver.quit();
    }
    
    private void injectjQueryIfNeeded(){
        if(!jQueryLoaded()){
            injectQuery();
        }
    }

    public boolean jQueryLoaded() {
        Boolean loaded;
        try{
            JavascriptExecutor js = (JavascriptExecutor) driver;
            loaded = (Boolean) js.executeScript("return jQuey()!=null");
        } catch(WebDriverException e){
            loaded = false;
        }
        return loaded;
    }

    public void injectQuery() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("var headID = document.getElementByTagName(\"head\")[0];"
                + "var newScript = document.createElement('script');"
                + "newScript.type = 'text/javascript';"
                + "newScript.src = 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js';"
                + "headID.appendChild(newScript);");
    }
    
    private boolean isElementPresent(By by){
        try{
            driver.findElement(by);
        }catch(NoSuchElementException e){
            return false;
        }
        return true;
    }
    
}
