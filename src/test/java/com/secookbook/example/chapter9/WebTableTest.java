
package com.secookbook.example.chapter9;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class WebTableTest {
    
    WebDriver driver;
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }
    
    @Test
    public void testWebTableTest(){
        driver.get("http://cookbook.seleniumacademy.com/Locators.html");
        
        WebTable table = new WebTable(driver.findElement(By.cssSelector("div.cart-info table")));
        
        assertEquals(3,table.getRowCount());
        assertEquals(5,table.getColumnCount());
        assertEquals("iPhone",table.getCellData(3, 1));
        
        WebElement cellEdit = table.getCellEditor(3, 3, 0);
        cellEdit.clear();
        cellEdit.sendKeys("2");
    }
    
    @After
    public void tearDown(){
        driver.quit();
    }
    
}
