/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secookbook.example.chapter9;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class ObjectMapTest {
    
    private WebDriver driver;
    private ObjectMap map;
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }
    
    @Test
    public void testBmiCalculatorPerformance() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/bmicalculator.html");
        map = new ObjectMap("/src/test/resources/objectmap/objectmap.properties");
        
        WebElement height = driver.findElement(map.getLocator("height_field"));
        height.sendKeys("181");
        
        WebElement weight = driver.findElement(map.getLocator("weight_field"));
        weight.sendKeys("80");
        
        WebElement calculateButton = driver.findElement(map.getLocator("calculate_field"));
        calculateButton.click();
        
        WebElement bmi = driver.findElement(map.getLocator("bmi_field"));
        bmi.click();
        assertEquals("24.4",bmi.getAttribute("value"));
    }
    
    @After
    public void tearDown(){
        driver.quit();
    }
    
}
