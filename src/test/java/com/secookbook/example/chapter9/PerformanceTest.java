/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secookbook.example.chapter9;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class PerformanceTest {
    
    private WebDriver driver;
    private ObjectMap map;
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }
    
    @Test
    public void testBmiCalculator() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/bmicalculator.html");
        
        JavascriptExecutor js = (JavascriptExecutor) driver;
        long loadEventEnd = (Long) js.executeScript("return window.performance.timing.loadEventEnd;");
        long navigationStart = (Long) js.executeScript("return window.performance.timing.navigationStart;");
        
        System.out.println("Page Load is "+ (loadEventEnd - navigationStart)/1000+" seconds.");
        
        //assertEquals("24.4",bmi.getAttribute("value"));
    }
    
    @After
    public void tearDown(){
        driver.quit();
    }
    
}
