
package com.secookbook.example.chapter9;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WrapsDriver;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class JQueryUITab {
    
    private WebElement _jQueryUITab;
    
    public JQueryUITab(WebElement jQueryUITab){
        set_jQueryUITab(jQueryUITab);
    }
    
    private WebElement get_jQueryUITab() {
        return this._jQueryUITab;
    }

    public void set_jQueryUITab(WebElement jQueryUITab) {
        this._jQueryUITab = jQueryUITab;
    }
    
    public int getTabCount(){
        List<WebElement> tabs = this._jQueryUITab.findElements(By.cssSelector(".ui-tabs-nav > li"));
        return tabs.size();
    }
    
    public String getSelectedTab(){
        WebElement tab = this._jQueryUITab.findElement(By.cssSelector(".ui-tabs-nav > li[class*='ui-tabs-selected']"));
        return tab.getText();
    }
    
    public void selectTab(String tabName){
        int idx=0;
        boolean found = false;
        List<WebElement> tabs = this._jQueryUITab.findElements(By.cssSelector(".ui-tabs-nav > li"));
        
        for(WebElement tab: tabs){
            if(tabName.equals(tab.getText().toString())){
                WrapsDriver wrappedElement = (WrapsDriver) this._jQueryUITab;
                JavascriptExecutor driver = (JavascriptExecutor) wrappedElement.getWrappedDriver();
                driver.executeScript("jQuery(arguments[0]).tabs().tabs('select',arguments[]1);",this._jQueryUITab,idx);
                found = true;
                break;
            }
            idx++;
        }
        if(found ==false){
            throw new IllegalArgumentException("Could not find tab '"+ tabName+"'");
        }
    }
}
