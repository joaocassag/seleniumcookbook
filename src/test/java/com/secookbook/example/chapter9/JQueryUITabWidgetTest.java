
package com.secookbook.example.chapter9;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class JQueryUITabWidgetTest {
    
    private WebDriver driver;
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }
    
    @Test
    public void testJQueryUITabWidget(){
        JQueryUITab tab = new JQueryUITab(driver.findElement(By.cssSelector("div[id=MyTab][class^=ui-tabs]")));
        
        assertEquals(3,tab.getTabCount());
        
        assertEquals("Home",tab.getSelectedTab());
        
        tab.selectTab("Options");
        assertEquals("Options",tab.getSelectedTab());
        
        tab.selectTab("Admin");
        assertEquals("Admin",tab.getSelectedTab());
        
        tab.selectTab("Home");
    }
    
    @After
    public void tearDown(){
        driver.quit();
    }
    
}
