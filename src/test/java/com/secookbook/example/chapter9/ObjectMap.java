
package com.secookbook.example.chapter9;

import java.io.FileInputStream;
import java.util.Properties;
import org.openqa.selenium.By;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class ObjectMap {
    
    Properties properties;
    
    public ObjectMap(String mapFile){
        properties = new Properties();
        try{
            FileInputStream in = new FileInputStream(mapFile);
            properties.load(in);
            in.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public By getLocator(String le) throws Exception{
        String locator = properties.getProperty(le);
        String locatorType = locator.split(">")[0];
        String locatorValue = locator.split(">")[1];
        
        if(locatorType.toLowerCase().equals("id"))
            return By.id(locatorValue);
        else if(locatorType.toLowerCase().equals("name"))
            return By.name(locatorValue);
        else if(locatorType.toLowerCase().equals("classname") || locatorType.toLowerCase().equals("class"))
            return By.className(locatorValue);
        else if(locatorType.toLowerCase().equals("tagname") || locatorType.toLowerCase().equals("tag"))
            return By.tagName(locatorValue);
        else if(locatorType.toLowerCase().equals("linktext") || locatorType.toLowerCase().equals("link"))
            return By.linkText(locatorValue);
        else if(locatorType.toLowerCase().equals("partiallinktext"))
            return By.partialLinkText(locatorValue);
        else if(locatorType.toLowerCase().equals("csselector") || locatorType.toLowerCase().equals("css"))
            return By.cssSelector(locatorValue);
        else if(locatorType.toLowerCase().equals("xpath"))
            return By.xpath(locatorValue);
        else 
            throw new Exception("Locator type '"+locatorType+"' not defined!");
    }
    
}
