
package com.secookbook.example.chapter11;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin={"pretty",
"html:target/report/report.html",
        "json:target/report/cucu_json_report.json",
        "junit:target/report/cucumber_junit_report.xml"})
public class RunCukesTest {
    
}
