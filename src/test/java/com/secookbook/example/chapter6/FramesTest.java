
package com.secookbook.example.chapter6;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
//import com.gargoylesoftware.htmlunit.BrowserVersion;
import org.openqa.selenium.internal.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class FramesTest {
    
    static WebDriver driver;
    //static HtmlUnitDriver driver;
    
    @BeforeClass
    public static void setUp(){
        //TODO: select driver based on platform
        
         //driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_78);
        
        //Firefox driver
        System.setProperty("webdriver.gecko.driver","src/drivers/geckodriver_mac");
        FirefoxOptions o = new FirefoxOptions();
        o.setHeadless(true);
        driver = new FirefoxDriver(o);
        
        //driver = new RemoteWebDriver(capabilities);
        //driver = new ChromeDriver();
        driver.manage().window().maximize();
        
    }
    
    @Test
    @Ignore
    public void testWithIdOrName() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Frames.html");
        //driver.findElement(By.id("simple")).click();
        //WebElement left = driver.findElement(By.id("left"));
        
        
        try{
            //new WebDriverWait (driver,5).until(ExpectedConditions.presenceOfElementLocated(By.id("left")));
            driver.switchTo().frame("left");
            
            WebElement message = driver.findElement(By.tagName("p"));
            assertEquals("This is Left Frame",message.getText());
        }finally{
            driver.switchTo().defaultContent();
        }
        
        try{
            driver.switchTo().frame("right");
            
            WebElement message = driver.findElement(By.tagName("p"));
            assertEquals("This is Right Frame",message.getText());
        }finally{
            driver.switchTo().defaultContent();
        }
        
    }
    
    @Test
    @Ignore
    public void testFrameByIndex() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Frames.html");
        //driver.findElement(By.id("simple")).click();
        //WebElement left = driver.findElement(By.id("left"));
        
        
        try{
            //new WebDriverWait (driver,5).until(ExpectedConditions.presenceOfElementLocated(By.id("left")));
            driver.switchTo().frame(1);
            
            WebElement message = driver.findElement(By.tagName("p"));
            assertEquals("This Frame doesn't have id or name",message.getText());
        }finally{
            driver.switchTo().defaultContent();
        }
    }
    
    @Test
    @Ignore
    public void testFrameByContents() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Frames.html");
        //driver.findElement(By.id("simple")).click();
        List<WebElement> frames = driver.findElements(By.tagName("frame"));
        
        try{
            for(WebElement frame: frames){
                driver.switchTo().frame(frame);
                String title = driver.getTitle();
                if(title.equals("Frame B")){
                    WebElement message = driver.findElement(By.tagName("p"));
                    assertEquals("This is Left Frame", message.getText());
                    break;
                } else{
                    driver.switchTo().defaultContent();
                }
            }
        }
        finally{
            driver.switchTo().defaultContent();
        }
    }
    
    @Test
    @Ignore
    public void testIFrame() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Frames.html");
        String currentWindow = driver.getWindowHandle();
        
        try{
            driver.switchTo().frame("right");
            
            WebElement t = driver.findElement(By.tagName("iframe"));
            try{
                driver.switchTo().frame(t);
                WebElement button = driver.findElement(By.id("follow-button"));
                button.click();
                try{
                    for(String windowId: driver.getWindowHandles()){
                        driver.switchTo().frame(windowId);
                        if(driver.getTitle().equals("Unmesh Gundecha (@upgundecha) on Twitter")){
                            assertTrue("Twitter Login Popup Window Found",true);
                            driver.close();
                            break;
                        }
                    }
                }finally{
                    driver.switchTo().frame(currentWindow);
                }
            }finally{
                driver.switchTo().defaultContent();
            }
        }finally{
            driver.switchTo().defaultContent();
        }
    }
    
    @AfterClass
    public static void tearDown() throws Exception{
        //driver.close();
        driver.quit();
    }
    
}
