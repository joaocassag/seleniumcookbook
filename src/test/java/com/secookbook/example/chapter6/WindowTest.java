/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secookbook.example.chapter6;

import java.util.concurrent.TimeUnit;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class WindowTest {
    
    static WebDriver driver;
    //static HtmlUnitDriver driver;
    
    @BeforeClass
    public static void setUp(){
        //TODO: select driver based on platform
        
         //driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_78);
        
        //Firefox driver
        System.setProperty("webdriver.gecko.driver","src/drivers/geckodriver_mac");
        FirefoxOptions o = new FirefoxOptions();
        //o.setHeadless(true);
        driver = new FirefoxDriver(o);
        
        //driver = new RemoteWebDriver(capabilities);
        //driver = new ChromeDriver();
        driver.manage().window().maximize();
        
    }
    
    @Test
    @Ignore
    public void testWindowsUsingName() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Config.html");
        
        String parentWindowID = driver.getWindowHandle();
        //driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        driver.findElement(By.id("helpbutton")).click();
        try{
            driver.switchTo().window("HelpWindow");
            try{
                System.out.println("TITLE#1 is:" + driver.getTitle());
                assertEquals("Help",driver.getTitle());
            }finally{
                driver.close();
            }
        }finally{
            driver.switchTo().window(parentWindowID);
        }
        assertEquals("Build my Car - Configuration",driver.getTitle());
    }
    
    @Test
    @Ignore
    public void testWindowsUsingTitle() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Config.html");
        String parentWindowID = driver.getWindowHandle();
        driver.findElement(By.id("visitbutton")).click();
        
        try{
            for(String windowId: driver.getWindowHandles()){
                String title = driver.switchTo().window(windowId).getTitle();
                if(title.equals("Visit Us")){
                    assertEquals("Visit Us",driver.getTitle());
                    driver.close();
                    break;
                }
            }
        }finally{
            driver.switchTo().window(parentWindowID);
        }
        assertEquals("Build my Car - Configuration",driver.getTitle());
    
    }
    
    @Test
    @Ignore
    public void testWindowsContents() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Config.html");
        String parentWindowID = driver.getWindowHandle();
        driver.findElement(By.id("chatbutton")).click();
        
        try{
           for(String windowId: driver.getWindowHandles()){
               driver.switchTo().window(windowId);
               
               
               String ps = driver.getPageSource();
               
               if(ps.contains("Configuration - Online Chat")){
                   assertTrue(driver.findElement(By.tagName("p")).getText().
                           equals("Wait while we connect you to Chat..."));
                   
                   driver.findElement(By.id("closebutton")).click();
                   break;
               }
           } 
        }finally{
            driver.switchTo().window(parentWindowID);
        }
        assertEquals("Build my Car - Configuration",driver.getTitle());
    }
    
    @AfterClass
    public static void tearDown() throws Exception{
        //driver.close();
        driver.quit();
    }
    
}
