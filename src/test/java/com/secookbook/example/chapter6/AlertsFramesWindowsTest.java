
package com.secookbook.example.chapter6;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class AlertsFramesWindowsTest {
    
    static WebDriver driver;
    
    @BeforeClass
    public static void setUp(){
        //TODO: select driver based on platform
        System.setProperty("webdriver.gecko.driver","src/drivers/geckodriver_mac");
        
        driver = new FirefoxDriver();
        //driver = new RemoteWebDriver(capabilities);
        //driver = new ChromeDriver();
        driver.manage().window().maximize();
        
    }
    
    @Test
    @Ignore
    public void testSimpleAlert() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Alerts.html");
        driver.findElement(By.id("simple")).click();
        
        new WebDriverWait (driver,5).until(ExpectedConditions.alertIsPresent());
        
        Alert alert = driver.switchTo().alert();
        
        String textOnAlert = alert.getText();
        
        assertEquals("Hello! I am an alert box!", textOnAlert);
        
        alert.accept();
    }
    
    @Test
    @Ignore
    public void testConfirmAccept() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Alerts.html");
        driver.findElement(By.id("confirm")).click();
        
        Alert alert = driver.switchTo().alert();
        alert.accept();
        
        WebElement message = driver.findElement(By.id("demo"));
        assertEquals("You Accepted Alert!",message.getText());
    }
    
    @Test
    @Ignore
    public void testConfirmDismiss() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Alerts.html");
        driver.findElement(By.id("confirm")).click();
        
        Alert alert = driver.switchTo().alert();
        alert.dismiss();
        
        WebElement message = driver.findElement(By.id("demo"));
        assertEquals("You Dismissed Alert!",message.getText());
    }
    
    @Test
    @Ignore
    public void testPrompt() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/Alerts.html");
        driver.findElement(By.id("prompt")).click();
        
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("Foo");
        alert.accept();
        
        WebElement message = driver.findElement(By.id("prompt_demo"));
        assertEquals("Hello Foo! How are you today?",message.getText());
    }
    
    @AfterClass
    public static void tearDown() throws Exception{
        driver.quit();
    }
    
}
