
package com.secookbook.example.chapter7;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
@RunWith(Parameterized.class)
public class SimpleDDTTest {
    
    private static WebDriver driver;
    
    private String height;
    private String weight;
    private String bmi;
    private String bmiCategory;
    
    @Parameters
    public static List<String[]> testData(){
        return Arrays.asList(
            new String[][]{
                {"160","45","17.6","Underweight"},
                {"168","70","24.8","Normal"},
                {"181","89","27.2","Overweight"},
                {"178","100","31.6","Obesity"}
            }
        );
    }
    
    @BeforeClass
    public static void setUp(){
        //TODO: select driver based on platform
        
         //driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_78);
        
        //Firefox driver
        System.setProperty("webdriver.gecko.driver","src/drivers/geckodriver_mac");
        FirefoxOptions o = new FirefoxOptions();
        //o.setHeadless(true);
        driver = new FirefoxDriver(o);
        
        //driver = new RemoteWebDriver(capabilities);
        //driver = new ChromeDriver();
        driver.manage().window().maximize();
        
    }
    
    public SimpleDDTTest(String height, String weight, String bmi, String bmiCategory){
        this.height = height;
        this.weight = weight;
        this.bmi = bmi;
        this.bmiCategory = bmiCategory;
    }
    
    @Test
    @Ignore
    public void testBMICalculator() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/bmicalculator.html");
        
        WebElement heightField = driver.findElement(By.name("heightCMS"));
        heightField.clear();
        heightField.sendKeys(height);
        
        WebElement weightField = driver.findElement(By.name("weightKg"));
        weightField.clear();
        weightField.sendKeys(weight);
        
        WebElement cB = driver.findElement(By.id("Calculate"));
        cB.click();
        
        WebElement bmiLabel = driver.findElement(By.name("bmi"));
        assertEquals(bmi,bmiLabel.getAttribute("value"));
        
        WebElement bmiCategoryLabel = driver.findElement(By.name("bmi_category"));
        assertEquals(bmiCategory,bmiCategoryLabel.getAttribute("value"));
        
    }
    
    @AfterClass
    public static void tearDown() throws Exception{
        //driver.close();
        driver.quit();
    }
    
}
