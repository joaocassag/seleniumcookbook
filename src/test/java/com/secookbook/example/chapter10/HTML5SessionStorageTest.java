
package com.secookbook.example.chapter10;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class HTML5SessionStorageTest {
    
    private WebDriver driver;
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }
    
    @Test
    public void testHTML5SessionStorage() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/html5storage.html");
        WebElement cb = driver.findElement(By.id("click"));
        WebElement cf = driver.findElement(By.id("clicks"));
        
        
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String clickCount = (String) js.executeScript("return sessionStorage.clickcount;");
        assertEquals(null,clickCount);
        assertEquals("0",cf.getAttribute("value"));
        
        cb.click();
        
        clickCount = (String) js.executeScript("return sessionStorage.clickcount;");
        assertEquals("1",clickCount);
        assertEquals("1",cf.getAttribute("value"));
        
        //removeItem = (String) js.executeScript("return sessionStorage.removeItem(lastname);");
        //removeItem = (String) js.executeScript("return sessionStorage.clear();");
    }
    
    @After
    public void tearDown(){
        driver.quit();
    }
    
}
