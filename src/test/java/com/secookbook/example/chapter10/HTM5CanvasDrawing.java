
package com.secookbook.example.chapter10;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class HTM5CanvasDrawing {
    
    private WebDriver driver;
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }
    
    @Test
    public void testHTML5CanvasDrawing() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/html5canvasdraw.html");
        
        WebElement canvas = driver.findElement(By.id("imageTemp"));
        
        Select drawTools = new Select(driver.findElement(By.id("dtool")));
        drawTools.selectByValue("pencil");
        
        Actions builder =  new Actions(driver);
        builder.clickAndHold(canvas).
                moveByOffset(-50, -10).
                release().
                perform();
        
        //FileUtils.copyFile(srcFile, destFile);
        //TODO:
       
    }
    
    @After
    public void tearDown(){
        driver.quit();
    }
    
}
