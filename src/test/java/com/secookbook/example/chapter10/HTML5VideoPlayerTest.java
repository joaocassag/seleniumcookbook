
package com.secookbook.example.chapter10;

import java.io.File;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.apache.commons.io.FileUtils;
import org.junit.Ignore;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class HTML5VideoPlayerTest {
    
    private WebDriver driver;
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }
    
    @Test
    public void testHTML5VideoPlayer() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/html5video.html");
        
        WebElement vp = driver.findElement(By.id("vplayer"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String source = (String) js.executeScript("return arguments[0].currentSrc;",vp);
        long duration = (Long) js.executeScript("return arguments[0].duration;",vp);
        
        assertEquals("http://html5demos.com/assets/dizzy.mp4",source);
        assertEquals(25,duration);
        
        js.executeScript("return arguments[0].play();",vp);
        //js.executeScript("var vid =  document.getElementById('vplayer'); return vid.play();",vp);
        //document.getElementById('vplayer').play()
        
        
        Thread.sleep(5000);
        js.executeScript(" return arguments[0].pause();",vp);
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(srcFile,new File("target/screenshots/pause_play.png"));
    }
    
    @After
    public void tearDown(){
        driver.quit();
    }
    
    
    
}
