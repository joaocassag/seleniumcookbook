
package com.secookbook.example.chapter10;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author <joaocassa@gmail.com>
 */
@Ignore
public class HTML5LocalStorageTest {
    
    private WebDriver driver;
    
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }
    
    @Test
    public void testHTML5LocalStorage() throws Exception{
        driver.get("http://cookbook.seleniumacademy.com/html5storage.html");
        
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String lastname = (String) js.executeScript("return localStorage.lastname;");
        assertEquals("Smith",lastname);
    
    }
    
    @After
    public void tearDown(){
        driver.quit();
    }
    
}
