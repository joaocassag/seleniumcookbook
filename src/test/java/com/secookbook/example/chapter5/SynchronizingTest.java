
package com.secookbook.example.chapter5;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class SynchronizingTest {
    private WebDriver driver;
    
    @Before
    public void setUp(){
        //TODO: select driver based on platform
        System.setProperty("webdriver.gecko.driver","src/drivers/geckodriver_mac");
        
        driver = new FirefoxDriver();
        //driver = new RemoteWebDriver(capabilities);
        //driver = new ChromeDriver();
        driver.manage().window().maximize();
        
    }
    
    @Test
    @Ignore
    public void testWithImplicitWait(){
        driver.get("http://cookbook.seleniumacademy.com/AjaxDemo.html");
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        
        driver.findElement(By.linkText("Page 4")).click();
        //driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);

        WebElement message = driver.findElement(By.id("page4"));
        assertTrue(message.getText().contains("Nunc nibh tortor"));
        
    }
    
    @Test
    @Ignore
    public void testWithExplicitWait(){
        driver.get("http://www.google.com");
        
        WebElement query = driver.findElement(By.name("q"));
        query.sendKeys("selenium");
        //query.click();
        query.submit();
        
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.titleContains("selenium"));
        
        assertTrue(driver.getTitle().toLowerCase().startsWith("selenium"));
    }
    
    @Test
    @Ignore
    public void testWithExplicitWaitWithCommonConditions(){
        driver.get("http://cookbook.seleniumacademy.com/AjaxDemo.html");
        
        driver.findElement(By.linkText("Page 4")).click();
        WebElement message = new WebDriverWait(driver,10).until(
                new ExpectedCondition<WebElement>(){
                    public WebElement apply(WebDriver d){
                        return d.findElement(By.id("page4"));
                    }
        });
        assertTrue(message.getText().contains("Nunc nibh tortor"));
    }
    
    @Test
    @Ignore
    public void testOutlookEmailInputElement(){
        driver.get("https://outlook.office.com/");
        
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.presenceOfElementLocated((By.id("i0116"))));
        assertTrue(driver.findElement(By.id("i0116")).getAttribute("placeholder").contains("Email"));
        
        /*
        //driver.findElement(By.linkText("Link")).click();
        WebElement message = new WebDriverWait(driver,5).until(
                new ExpectedCondition<WebElement>(){
                    public WebElement apply(WebDriver d){
                        return d.findElement(By.id("i0116"));
                    }
        });
        //System.out.println("#1 Source:\n"+driver.getPageSource());
        //assertTrue(message.getText().contains("Sample text"));
        */
        //assertTrue(message.getAttribute("placeholder").contains("Email"));
    }
    
    @Test
    @Ignore
    public void testDOMEvent(){
        driver.get("http:/www.google.com");
        
        //...
        
        //WebElement message = 
        new WebDriverWait(driver,5).until(
            new ExpectedCondition<Boolean>(){
                public Boolean apply(WebDriver d){
                    JavascriptExecutor js = (JavascriptExecutor) d;
                    return (Boolean) js.executeScript("return jQuery.active == 0");
                }
        });
        //assertTrue(message.getAttribute("placeholder").contains("Email"));
    }
    
    @Test
    @Ignore
    public void testFluentWait(){
        driver.get("http://cookbook.seleniumacademy.com/AjaxDemo.html");
        driver.findElement(By.linkText("Page 4")).click();
        
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(2))
                .pollingEvery(Duration.ofSeconds(3))
                .ignoring(NoSuchElementException.class);
        WebElement message = wait.until( 
            new Function<WebDriver,WebElement>(){
                public WebElement apply(WebDriver d){
                    return d.findElement(By.id("page4"));
                }
        });
        assertTrue(message.getText().contains("Nunc nibh tortor"));
    }
    
    @Test
    @Ignore
    public void testFluentWaitWithPredicate(){
        driver.get("http://cookbook.seleniumacademy.com/AjaxDemo.html");
        //driver.findElement(By.linkText("Page 4")).click();
        
        FluentWait<By> wait = new FluentWait<By>(By.linkText("Page 4"))
                .withTimeout(Duration.ofSeconds(2))
                .pollingEvery(Duration.ofSeconds(3))
                .ignoring(NoSuchElementException.class);
        
        
        //TODO: add predicate??
        /*
        //wait.until(new Predidcate<By>(){
            public boolean apply(By by){
                   try{
                       return driver.findElement(by).isDisplayed();
                   }catch(NoSuchElementException e){
                       return false;
                   }
              
            }
        });
        */
        driver.findElement(By.linkText("Page 4")).click();
    }
    
    @Test
    @Ignore
    public void testWithExplicitWaitC19(){
        //FirefoxOptions o = new FirefoxOptions();
        //o.setHeadless(true);
        //driver = new FirefoxDriver(o);
        int count = 0;
        while(count<5){
            try{
                driver.get("https://www.mhealthappointments.com/covidappt");

                WebDriverWait wait = new WebDriverWait(driver,5);
                wait.until(ExpectedConditions.presenceOfElementLocated((By.id("covid_vaccine_search_input"))));
                //assertTrue(driver.findElement(By.id("i0116")).getAttribute("placeholder").contains("Email"));
                System.out.println("#finding zipcode input");

                WebElement mileradiobutton = driver.findElement(By.id("fiveMile-covid_vaccine_search"));
                if(!mileradiobutton.isSelected()){
                    mileradiobutton.click();
                    System.out.println("#clicked five miles radio button");

                }
                System.out.println("#search within five miles");

                WebElement zipcode = driver.findElement(By.id("covid_vaccine_search_input"));
                System.out.println("#found zipcode input");

                zipcode.sendKeys("60637");
                System.out.println("#filled zipcode");

                //zipcode.submit();
                //System.out.println("#submit");

                WebElement searchbutton = driver.findElement(By.className("company-search-btn"));
                searchbutton.click();
                System.out.println("#submit");

                wait.until(ExpectedConditions.presenceOfElementLocated((By.id("storeList1600108750567"))));

                WebElement nearest = driver.findElement(By.id("storeList1600108750567"));
                System.out.println("#found list");
                nearest.click();
                System.out.println("#clicked list");

                wait.until(ExpectedConditions.presenceOfElementLocated((By.id("covid_vaccine_search_error"))));
                WebElement error = driver.findElement(By.id("covid_vaccine_search_error"));
                System.out.println("#retrieving error");
                String errorText = error.getAttribute("aria-label");

                System.out.println("ERROR TEXT: "+errorText);
                if(errorText!=null && errorText.contains("Currently, all appointments are booked in your area")){
                    System.out.println("No appointments");
                }
                count++;
                System.out.println("increment counter: "+count);

                //driver.close();
                //driver.quit();
            }catch(Exception e){
                System.out.println("An error occured due to: "+e.getMessage());
            }
        }
        //covid_vaccine_search_error
        /*
        Currently, all appointments are booked in your area. As more vaccines become available, additional appointments will open up. As we work to keep everyone safe, we ask that you please not call your local store; our pharmacy staff will be focused on serving patients and won't have any additional information.
        */
        
    /*
    <div class="col-sm-12 mt-1 store-list-row" style="padding: 0px;" 
    onkeyup="checkKeyPressAndAssessment( 1600108750567, 'COVID_VACCINE_SEARCH_TYPE_ZIP', '60637', event)" 
    onmouseup="loadAssessmentForStore( 1600108750567, 'COVID_VACCINE_SEARCH_TYPE_ZIP', '60637')">
        <div class="col-7 col-sm-8 col-md-9 py-2" style="float:left;">
        <input type="radio" id="storeList1600108750567" name="storeList" value="" 
            style="position:absolute;left:5px;top:50%;transform:translateY(-50%);" 
            aria-label="No availability at location, Jewel-Osco 3082 - 6014 S COTTAGE GROVE AVE, Chicago, IL, 60637" 
            onkeyup="checkKeyPressAndAssessment( 1600108750567, 'COVID_VACCINE_SEARCH_TYPE_ZIP', '60637', event)" 
            onmouseup="loadAssessmentForStore( 1600108750567, 'COVID_VACCINE_SEARCH_TYPE_ZIP', '60637')">
        <label for="storeList1600108750567" aria-hidden="true" style="padding-left:10px" 
            onclick="loadAssessmentForStore( 1600108750567, 'COVID_VACCINE_SEARCH_TYPE_ZIP', '60637')">
            Jewel-Osco 3082 - 6014 S COTTAGE GROVE AVE, Chicago, IL, 60637</label>
        </div>
        <div class="col-5 col-sm-4 col-md-3 py-2 text-center" style="float:right;" tabindex="-1" aria-hidden="true">
        <span class="text-capitalize" aria-hidden="true">no</span></div></div>
    */
    /*
    <div class="col-sm-12 mt-1 store-list-row" style="padding: 0px;" 
    onkeyup="checkKeyPressAndAssessment( 1600108203429, 'COVID_VACCINE_SEARCH_TYPE_ZIP', '60637', event)" 
    onmouseup="loadAssessmentForStore( 1600108203429, 'COVID_VACCINE_SEARCH_TYPE_ZIP', '60637')">
        <div class="col-7 col-sm-8 col-md-9 py-2" style="float:left;">
        <input type="radio" id="storeList1600108203429" name="storeList" value="" 
        style="position:absolute;left:5px;top:50%;transform:translateY(-50%);" 
        aria-label="No availability at location, Jewel-Osco 0230 - 7530 Stony Island, Chicago, IL, 60649" 
        onkeyup="checkKeyPressAndAssessment( 1600108203429, 'COVID_VACCINE_SEARCH_TYPE_ZIP', '60637', event)" 
        onmouseup="loadAssessmentForStore( 1600108203429, 'COVID_VACCINE_SEARCH_TYPE_ZIP', '60637')">
    <label for="storeList1600108203429" aria-hidden="true" style="padding-left:10px" 
        onclick="loadAssessmentForStore( 1600108203429, 'COVID_VACCINE_SEARCH_TYPE_ZIP', '60637')">
        Jewel-Osco 0230 - 7530 Stony Island, Chicago, IL, 60649</label>
    </div>
    <div class="col-5 col-sm-4 col-md-3 py-2 text-center" style="float:right;" tabindex="-1" aria-hidden="true">
    <span class="text-capitalize" aria-hidden="true">no</span></div></div>
    */
        
        //WebDriverWait wait = new WebDriverWait(driver,10);
        //wait.until(ExpectedConditions.titleContains("selenium"));
        
        //assertTrue(driver.getTitle().toLowerCase().startsWith("selenium"));
    }
    
    
    @After
    public void tearDown() throws Exception{
        driver.quit();
    }
    
}
