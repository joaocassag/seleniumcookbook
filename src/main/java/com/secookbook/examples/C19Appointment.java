
package com.secookbook.examples;

import java.time.Duration;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class C19Appointment extends Thread {
    private WebDriver driver;
    //Date now = new Date();
    boolean found=false;
    int counter=0;
    long SLEEP=60*5;
    
    
    public void setUp(){
        //TODO: select driver based on platform
        //System.setProperty("webdriver.gecko.driver","src/drivers/geckodriver_mac");
        System.setProperty("webdriver.gecko.driver","/etc/seleniumdriver/geckodriver_mac");
        
        //driver = new FirefoxDriver();
        FirefoxOptions o = new FirefoxOptions();
        o.setHeadless(true);
        driver = new FirefoxDriver(o);
        
        driver.manage().window().maximize();
        
    }

    public void run(){
        C19Appointment c19Appt = new C19Appointment();
        while(!found) {
            try {
                c19Appt.c19AppointmentAvailability();
                //if(found) break;
                counter++;
                log("counter: "+counter);
                log("sleeping for "+SLEEP+" seconds");
                sleep(SLEEP*1000);
                //if(found) break;
            } catch (Exception e) {
                log("An error occured due to: " + e.getMessage());
            }

            //if(found)
            //email user to let them know that there is a slot available
            //and set found to false again to try to find a new slot

        }


    }
    
    //@Ignore
    public void c19AppointmentAvailability() throws Exception{
        setUp();
        Date now = new Date();
        int countSuccess = 0;
        while(countSuccess<1){
            try{
                driver.get("https://www.mhealthappointments.com/covidappt");

                WebDriverWait wait = new WebDriverWait(driver,15);
                wait.until(ExpectedConditions.presenceOfElementLocated((By.id("covid_vaccine_search_input"))));
                //assertTrue(driver.findElement(By.id("i0116")).getAttribute("placeholder").contains("Email"));
                log("#finding zipcode input");

                WebElement mileradiobutton = driver.findElement(By.id("fiveMile-covid_vaccine_search"));
                if(!mileradiobutton.isSelected()){
                    mileradiobutton.click();
                    //log("#clicked five miles radio button");
                    log("#clicked five miles radio button");

                }
                log("#search within five miles");

                WebElement zipcode = driver.findElement(By.id("covid_vaccine_search_input"));
                log("#found zipcode input");

                zipcode.sendKeys("60637");
                log("#filled zipcode");

                //zipcode.submit();
                //log("#submit");

                WebElement searchbutton = driver.findElement(By.className("company-search-btn"));
                searchbutton.click();
                log("#submit");

                wait.until(ExpectedConditions.presenceOfElementLocated((By.id("storeList1600108750567"))));

                WebElement nearest = driver.findElement(By.id("storeList1600108750567"));
                log("#found list");
                nearest.click();
                log("#clicked list");

                wait.until(ExpectedConditions.presenceOfElementLocated((By.id("covid_vaccine_search_error"))));
                WebElement error = driver.findElement(By.id("covid_vaccine_search_error"));
                log("#retrieving error");
                String errorText = error.getAttribute("aria-label");

                //log("ERROR TEXT: "+errorText);
                if(errorText!=null && errorText.contains("Currently, all appointments are booked in your area")){
                    log("No appointments");
                } else{
                    found=true;
                    log("Found available slot appointments");
                }
                countSuccess++;
                log("increment countSuccess: "+countSuccess);
            }catch(Exception e){
                log("An error occured due to: "+e.getMessage());
                tearDown();
                setUp();
                
            }
        }
        tearDown();
    }
    
    public void tearDown() throws Exception{
        driver.quit();
    }
    
    public void log(String msg){
        //now = new Date();
        System.out.println(new Date()+" "+msg);
    }
    
    public static void main(String[] args) {

        C19Appointment c19Appt = new C19Appointment();

        try {
            //c19Appt.setUp();
            c19Appt.start();
            //c19Appt.tearDown();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*
        C19Appointment c19Appt = new C19Appointment();
        try{
            c19Appt.c19AppointmentAvailability();
        }catch(Exception e){
            c19Appt.log("An error occured due to: "+e.getMessage());
        }*/
        
    }
    
}
